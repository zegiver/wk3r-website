## Development

1. Run sass compiler. It needs to be dart-sass because it's supports the relativley new `@use` directive. So to live compile sass while you are developing please run the following command:
    - `cd assets/sass && sass --watch main.sass ../css/main.css`

    this is also the reason why the css file is included instead of the sass file. Hugo does not uses dart-sass.

2. After that you can run hugo via `hugo server -D`
