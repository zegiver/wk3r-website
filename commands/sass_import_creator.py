import argparse
from argparse import Namespace
from pathlib import Path
from typing import Dict, List, Union
import sys


file_endings = ["sass"]


def init_parser() -> Namespace:
    parser = argparse.ArgumentParser(
        description="Create main.(sass|scss) for a given 7-1 directory."
    )
    parser.add_argument("--directory", help="directory to root sass folder")
    parser.add_argument("--out", help="output file")
    return parser.parse_args()


def create_sass_file(directory: Union[str, Path], output: Union[str, Path]) -> None:
    if isinstance(directory, str):
        directory = Path(directory)

    if isinstance(output, str):
        output = Path(output)

    if output.is_dir():
        print(f"output: {output} is not a path to a file.", file=sys.stderr)
        exit(1)

    if not directory.exists():
        print(f"directory: {directory} does not exist.", file=sys.stderr)
        exit(1)

    import_structure: Dict[str, List[Path]] = {}
    for sass_file in directory.glob(f"**/*.sass"):
        if sass_file.name == output.name:
            continue
        sass_directory_name = sass_file.parent.name
        if sass_directory_name in import_structure:
            import_structure[sass_directory_name].append(sass_file)
        else:
            import_structure[sass_directory_name] = [sass_file]

    with output.open("w") as out_file:
        print('@charset "utf-8"', file=out_file)
        for key, values in import_structure.items():
            print(f"/* {key} */", file=out_file)
            for value in values:
                print(f'@import "{key}/{value.name}"', file=out_file)


def main() -> None:
    args = init_parser()
    create_sass_file(directory=args.directory, output=args.out)


if __name__ == "__main__":
    main()