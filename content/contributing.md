---
title: "Contributing"
date: 2020-09-05T13:38:48+02:00
draft: false
---
## **_Become a Crewmember_**  
### 
Becoming a member of the WK3R Crew is by far the most approachable means of contributing to Project WK3R.  
There are various positions one can help out with in the Crew, and all Crewmembers are entitled to select benefits.  
For more information, please visit [**_this thread_**](https://forum.wk3r.de/d/21-join-the-wk3r-crew-today).  
### 
 
### 
 
## **_Become an Artist_**
### 
Up until this point, WK3R did not have much of a need for a logo, wallpapers, or fan art.  
As we are distancing ourselves further away from strictly being a WK emulator project however, we would like to see the project take on a more individual identity.  
We want it to be about your modern take on WK, not what WK once was. If you would like to design and contribute some of these art assets, we would greatly appreciate it!  
We will gladly feature your name in the client credits if we do end up using any of it.  
    
Preferred formats are svg for the logo (if possible) and png for all others.  
For the wallpaper, we prefer a 21:9 image, 2560x1080 resolution or higher, that can still "look right" even with side edges cropped for 16:9 screens.  
All submissions should be routed to the #fan-art channel on our Discord server.

### 
 
### 
 
## **_Become a Pumanati Member_**
### 
The Pumanati is Project WK3R's own quasi-club for Patreon subscribers. If you would like to financially support Project WK3R, please visit our [Patreon page](https://www.patreon.com/wk3r).  
All Patreon subscribers automatically become a member of the Pumanati. For now, this will only grant subscribers a Pumanati role in our Discord server reflective of their subscription tier.  
In the future, this will expand to include fixed cash currency and cosmetic item incentives each month on official Project WK3R public servers, to be elaborated upon at a future date.  
These in-game rewards will not "accumulate" in any way for early subscribers.  
  
Project WK3R has been Riuga and co.'s passion project for the past several years.   
Early on we were paying out of pocket for server rental costs, and these days we are barely breaking even from the generous handful of patrons we do have.  
Even while test servers are not being run, these servers are crucial for our CI/CD setup with Docker and Kubernetes, which help expedite the building and testing cycle.  
  
During development, we are only asking for $40 a month. Averaged out over the course of a year, this allows us to break even with annual domain costs, monthly server rent, and annual mail provider costs.  
It is completely optional to support us on Patreon once we hit the $40 mark each month; this monthly goal will only be increased once we are ready to expand our cluster to service the public.  
Any excess is split 40/60 between Riuga as financial recompense and Rai to be kept for future server-related immediate expenses.
