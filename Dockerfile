# You need to run "docker build ."

# build sass files first
FROM node:14-alpine as sass-compile-stage
COPY ./assets /src/assets
RUN npm install sass -g
RUN sass /src/assets/sass/main.sass /src/assets/css/main.css

# copy sass generated css file into the building-state and run hugo
FROM registry.gitlab.com/pages/hugo/hugo_extended:latest as building-stage
WORKDIR /src/website/
COPY . .
COPY --from=sass-compile-stage /src/assets/css /src/website/assets/css
RUN hugo

# copy generated static site into nginx static site folder
FROM nginx:stable-alpine
COPY --from=building-stage /src/website/public /usr/share/nginx/html
